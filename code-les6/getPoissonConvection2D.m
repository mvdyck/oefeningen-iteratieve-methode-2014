% =======================================================
% *** FUNCTION getPoissonConvection2D
% =======================================================
function [A,b] = getPoissonConvection2D( Nx, a, f )

  h = 1/(Nx+1);

  e = ones(Nx,1);
  I = speye(Nx);
  D = spdiags( [-e 2*e -e], [-1:1], Nx, Nx ) / h^2;

  CX = a(1) * spdiags( [-e e], [-1,1], Nx, Nx ) / h;
  CY = a(2) * spdiags( [-e e], [-1,1], Nx, Nx ) / h;

  A = kron(I,D+CX) + kron(D+CY,I);

  b = f * ones(Nx*Nx,1);

end
% =======================================================
% *** END FUNCTION getPoissonConvection2D
% =======================================================