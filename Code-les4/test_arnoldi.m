%% check wether AV = VH

diag = [ 1:9, 10:200 ]';
n = length(diag);
A = spdiags( diag, 0, n, n );
b = ones( n, 1 );
k = 20;
[V,H] = arnoldi( A, b, k );
A*V
V*H

difference = A*V-V*H

%% symmetric A results in tridiagonal structure

A = [1 1 1 1 0 0;
    1 2 1 1 1 1;
    1 1 3 1 1 0;
    1 1 1 4 1 2;
    0 1 1 1 5 2;
    0 1 0 2 2 3];

% A = [2 -1 0 0 0 0;
%     -1 2 -1 0 0 0;
%     0 -1 2 -1 0 0;
%     0 0 -1 2 -1 0;
%     0 0 0 -1 2 -1;
%     0 0 0 0 -1 2];
    
n = length(A);
b = ones( n, 1);

[V,H] = arnoldi( A, b);

A*V
V*H

difference = A*V-V*H

H = H
