% =========================================================================
% *** FUNCTION plotEigenFunctionsPoisson2D
% ***
% *** Plots the eigenfunctions of the discretized two-dimensional Poisson
% *** problem on (0,1)^2 with Dirichlet boundary conditions.
% ***
% *** The eigenfunctions are actually composed of two eigenfunctions of
% *** the one-dimensional problem (z = z_1 \otimes z_2)
% ***
% *** Input:
% ***      N......discretization parameter for each of the axes
% ***      k1.....y-parameter component for the eigenfunction
% ***      k2.....x-parameter component for the eigenfunction
% ***
% =========================================================================
function plotEigenFunctionsPoisson2D( N, k1, k2 )

  % actually get the eigenfunction corresponding to (k1,k2)
  i = (1:N);
  z1 = sin( k1*i*pi/(N+1) );
  z2 = sin( k2*i*pi/(N+1) );
  z = kron( z1, z2 );

  h = 1/(N+1);
  x = (0:h:1);

  subplot(2,2,1);
  stem(x,[0,z1,0], 'bx' );
  title('Eigenfunction z_1 of Poisson1D')

  subplot(2,2,2);
  stem(x,[0,z2,0], 'bx' );
  title('Eigenfunction z_2 of Poisson1D')

  subplot(2,2,3);
  plotPoisson2D( z );
  title('Eigenfunction z = z_1\otimes z_2 of Poisson2D')

end
% =========================================================================
% *** END FUNCTION plotEigenFunctionsPoisson2D
% =========================================================================