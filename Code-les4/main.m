%% ===========================================================
% *** FUNCTION main
% ===========================================================
%function main()

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % define problem
  diag = [1, 10:200 ]';
  n = length(diag);

  A = spdiags( diag, 0, n, n );
  b = ones( n, 1 );
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


   % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   % do the Arnoldi iteration
   k = 10;
   [V,H] = arnoldi( A, b, k );
 
   % plot Ritz-polynomial
   eigenVals = diag;
   ritzVals  = eig( H(1:k,1:k) )
   
   plotRitzPolynomial( ritzVals, eigenVals );
   %set(gca,'XLim',[0,200])   
   % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%
%  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % have CG run with a precomputed exact solution
  tol   = 1e-15;
  maxIt = n;
  u0    = zeros(n,1);
  uSol  = A\b;
  [ u, flag, relRes, iter, resVec, errVecA ] ...
                   = cgWithErr( A, b, tol, maxIt, u0, uSol );
               iter
% % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % do the plots
  figure;
  mm = iter;
  
  % plot ||r||_2 and ||e||_A 
  plot( (0:mm), errVecA(1:mm+1), 'or' );
  hold on
  plot( (0:mm), resVec(1:mm+1), 'xb' );
  set( gca, 'YScale', 'log' );
  set( gca, 'XLim', [0,70] )
  hleg1=legend('$ \| \| e^{k}\| \|_A $','$\|\|r^{k}\|\|_2$','Location','SouthWest','Interpreter','latex')
  %hleg1=legend('$ \| \| e^{k}\| \|_A $','$\|\|r^{k}\|\|_2$','Location','SouthWest','Interpreter','latex')
   set(hleg1,'Interpreter','latex');
  hold off
%pause();
  % plot ||e^{k+1}||_A / ||e^{k}||_A
  figure
 plotRelative( (0:iter), errVecA );
 set( gca, 'XLim', [0,80] )
  hleg2=legend('$\| \| e^(k+1) \| \|_A / \| \| e^{k} \| \|_A $','Location','SouthWest')
  set(hleg2,'Interpreter','latex');
 
  %matlab2tikz( 'cgB-relErr.tikz','height', '4cm', 'width', '62mm' );
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    hold on;
 
   kappa = max(abs(diag)) / min( abs( diag(15:end) ) );
   convRate = (sqrt(kappa)-1)/(sqrt(kappa)+1)
   plot( [0 iter], [convRate convRate], '-k' )
   hold off;
