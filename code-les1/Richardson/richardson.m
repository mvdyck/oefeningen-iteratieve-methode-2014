% =========================================================================
% *** FUNCTION richardson
% ***
% *** Richardson method.
% ***
% =========================================================================
function [ u, flag, relRes, iter, resVec ] = richardson( A, b, u0 )

  % initialization
  tol     = 1e-5;
  maxIter = 1e4;

  normB   = norm( b, 2 );
  u       = u0;
  res     = b - A*u;
  iter    = 0;

  resVec    = zeros( maxIter+1, 1 );
  resVec(1) = norm( res, 2 );

  % set default flag: everything all right
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIter

      if resVec(iter) <= relTol
          break
      end

      % the actual update;
      % formulating the thing with 'res' removes the need for *two*
      % matrix-vector-multiplications per step
      u = u + res;

      res            = b - A*u;
      resVec(iter+1) = norm( res, 2 );
      plot(u);
      pause(.1)
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % handle the output
  if iter>=maxIter
      flag = 1;
      warning ( 'richardson:maxIter', ...
                'Maximum number of iterations reached.' );
  end
  relRes = resVec(iter+1)/normB;
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end
% =========================================================================
% *** END FUNCTION richardson
% =========================================================================