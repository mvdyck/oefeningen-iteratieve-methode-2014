

% ===========================================================
% *** FUNCTION plotRitzPolynomial
% ***
% *** Plots the polynomial
% ***
% *** f(x) = (theta1 - x) * ... * (thetaN - x)
% ***      / (theta1*...*thetaN)
% ***
% ===========================================================
function plotRitzPolynomial( theta, Pneval )

  % number of points where it will be evaluated
  N = 1000;

  minX = min( 0, min(min(theta),min(Pneval)) );
  maxX = max( max(theta), max(Pneval) );

  minX = minX - 0.1*(maxX-minX);
  maxX = maxX + 0.1*(maxX-minX);

  h = (maxX-minX) / (N-1);

  x = (minX:h:maxX)
  y = ritzEval( x, theta );

  % plot x-axis
  plot( [minX, maxX], [0 0], 'k-' );
  hold on;

  % plot the polynomial
  plot( x , y, '-b' );

  % plot eval data
  y = ritzEval( Pneval , theta);
  hold on;
  plot( Pneval, y, 'or' );

  hold off;

  minY = min(y) - 0.1* (max(y)-min(y));
  maxY = max(y) + 0.1* (max(y)-min(y));
  axis( [minX, maxX, minY, maxY ] );
end