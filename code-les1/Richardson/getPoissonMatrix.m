% =======================================================
% *** FUNCTION getPoissonMatrix
% ***
% *** Returns the 1D-Poisson-matrix and a right hand side
% *** (with f=1) for any given discretization parameter
% *** N.
% ***
% =======================================================
function [A,b] = getPoissonMatrix( N )


  h = 1/(N+1);

  e = ones( N,1 );
  A = h^(-2) ...
    * spdiags( [-e 2*e -e], [-1,0,1], N, N );

  b = ones(N,1);

end
% =======================================================
% *** END FUNCTION getPoissonMatrix
% =======================================================