function y = ritzEval( x, theta )
    % evaluate the polynomial

    n = length(x);
    y = zeros(n,1);
    for k = 1:n
        y(k) = prod( theta-x(k) );
    end
    y = y / prod(theta); 
    y2=0;
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

% ===========================================================
% *** END FUNCTION plotRitzPolynomial
% ===========================================================

