%% Vraag b: 
N = 100;
h = 5/(N+1);
lambda = 5 +(1:N)'*h;
figure();plot(lambda);title('lambda');
A = spdiags(lambda,0,N,N);
b = ones(N,1);
%%
% The real solution is u*
ustar = b./lambda;
figure();plot(ustar);title('u*');
% the condition number
kappa = max(lambda)/min(lambda)
%%
% solve with CG
maxiter = N;
[u , res , w , p , rho] = mycg(A,b,maxiter);
figure;plot(u);
e = u - repmat(ustar,1,maxiter+1);  % generates matrix with k th error in column k
e_norm = sqrt(diag(e.'*A*e));       % vector with norm of k th error at place k
r_norm = sqrt(rho);                 % vector with norm of k th residual at place k

kmax = 50;                          % only plot area of interest
%% plot of error
figure
semilogy(0:kmax,e_norm(1:kmax+1),'kx-')
hold
%%
% error estimation based on polynomial ((7.5-z)/7.5)^k (eq. 2.6)
p = @(k,z) ((7.5-z)./7.5).^k;
% for all k: |p(k,.)| maximal in lambda_min and lambda_max
semilogy(0:kmax,e_norm(1)*abs(p(0:kmax,min(lambda))),'b-')
%%
% error estimation based on the condition number (eq. 2.10)
factor = @(k) 2*((sqrt(kappa)-1)/(sqrt(kappa)+1)).^k;
semilogy(0:kmax,e_norm(1)*factor(0:kmax),'r-')
%%
grid on
xlabel('iteration')
legend('||e|| = ||x-x^*||','polynomial','condition number')
title('Error estimation of CG')

%% plot of residual
figure
semilogy(0:kmax,r_norm(1:kmax+1),'kx-')
hold
%%
% residual estimation based on polynomial ((7.5-z)/7.5)^k
p = @(k,z) ((7.5-z)./7.5).^k;
% for all k: |p(k,.)| maximal in lambda_min and lambda_max
semilogy(0:kmax,r_norm(1)*sqrt(kappa)*abs(p(0:kmax,min(lambda))),'b-')
%%
% residual estimation based on the condition number
factor = @(k) 2*((sqrt(kappa)-1)/(sqrt(kappa)+1)).^k;
semilogy(0:kmax,r_norm(1)*sqrt(kappa)*factor(0:kmax),'r-')

grid on
xlabel('iteration')
legend('||r|| = ||b-Ax||','polynomial','condition number')
title('Residual estimation of CG')
%%
% plot of ratios
figure
hold on
% plot of ratio of subsequent error and residual
plot(e_norm(2:kmax)./e_norm(1:kmax-1),'bx-')
plot(r_norm(2:kmax)./r_norm(1:kmax-1),'rx-')

grid on
xlabel('iteration')
ylabel('convergence factor')
legend('conv factor ||e||','conv factor ||r||')
title('Convergence factor of error and residual of CG')

%% vraag b (ii)
N = 100;
h = 5/(N/2+1);
lambda = [5+(1:N/2)*h 15+(1:N/2)*h]';
A = spdiags(lambda,0,N,N);
b = ones(N,1);
figure;plot(lambda,'.');title('lamda')

ustar = b./lambda;
kappa = max(lambda)/min(lambda) % is bigger

% solve with CG
maxiter = N;
[u , res , w , p , rho] = mycg(A,b,maxiter);
e = u - repmat(ustar,1,maxiter+1);
e_norm = sqrt(diag(e.'*A*e));
r_norm = sqrt(rho);

kmax = 32;
% plot of error
figure
semilogy(0:kmax,e_norm(1:kmax+1),'kx-')
hold

% error estimation based on polynomial ((7.5-z)/7.5)^k
p = @(k,z) ((7.5-z)./7.5).^k;
% forall k: |p(k,.)| maximal in lambda_max
semilogy(0:kmax,e_norm(1)*abs(p(0:kmax,max(lambda))),'b-')

% error estimation based on the condition number
factor = @(k) 2*((sqrt(kappa)-1)/(sqrt(kappa)+1)).^k;
semilogy(0:kmax,e_norm(1)*factor(0:kmax),'r-')

grid on
xlabel('iteration')
legend('||e|| = ||x-x^*||','polynomial','condition number')
title('Error estimation of CG')
%%
% plot of residual
figure('Name','residuals L ]5,10[ ]15,20[')
semilogy(0:kmax,r_norm(1:kmax+1),'kx-')
hold

% residual estimation based on polynomial ((7.5-z)/7.5)^k
p = @(k,z) ((7.5-z)./7.5).^k;
% forall k: |p(k,.)| maximal in lambda_max
semilogy(0:kmax,r_norm(1)*sqrt(kappa)*abs(p(0:kmax,max(lambda))),'b-')

% residual estimation based on the condition number
factor = @(k) 2*((sqrt(kappa)-1)/(sqrt(kappa)+1)).^k;
semilogy(0:kmax,r_norm(1)*sqrt(kappa)*factor(0:kmax),'r-')

grid on
xlabel('iteration')
legend('||r|| = ||b-Ax||','polynomial','condition number')
title('Residual estimation of CG')

%% plot of ratios
figure('Name','convergence L ]5,10[ ]15,20[')
hold on
% plot of ratio of subsequent error and residual
plot(e_norm(2:kmax)./e_norm(1:kmax-1),'bx-')
plot(r_norm(2:kmax)./r_norm(1:kmax-1),'rx-')

grid on
xlabel('iteration')
ylabel('convergence factor')
legend('conv factor ||e||','conv factor ||r||')
title('Convergence factor of error and residual of CG')