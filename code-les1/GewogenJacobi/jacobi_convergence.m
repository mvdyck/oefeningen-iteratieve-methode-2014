
%% PoissonMatrix
a = 0; 
b = 1;
N = 8;
h = (b-a)/(N+1);
x = linspace(a+h,b-h,N).';
e = ones(N,1);
u0 = zeros(N,1);

A = -1/h^2*spdiags([e -2*e e],[-1 0 1],N,N);
full(A)

%% iteration matrix
omega = 2/3;
Dinv = spdiags(1./spdiags(A,0),0,N,N);
I = speye(N);
R = (I-omega*(Dinv*A));
full(R)

%% eigenvalues/vector
omega = 1;
Dinv = spdiags(1./spdiags(A,0),0,N,N);
I = speye(N);
R = (I-omega*(Dinv*A));
% eigenvalues/vectors: analytical
va = zeros(N,N);
da = zeros(N,1);
for k = 1:N
va(:,N-k+1) = sin(k*pi*x)/norm(sin(k*pi*x));
da(N-k+1) = 1 -2*omega*sin(k*pi/(2*(N+1))).^2;
end
da

% eigenvalues/vectors: numeric
[v,d] = eig(full(R));
d

figure('Name','Eigenvectors of weigthed Jacobi')
for ind = 1:8
    subplot(4,2,ind)
    plot(x,va(:,N-ind+1),'kx-',x,v(:,N-ind+1),'bo-')
    xlabel('x')
    legend('analytic','numeric')
    grid on
end

%% Weighted Jacobi f = x.*(1-x)
f = x.*(1-x);
% analytical solution
ua = 1/12*x.^4 -1/6*x.^3 +1/12*x;

maxiter = 1000;
% weighted Jacobi
omega = 1;
[ u, flag, relRes, iter, resVec ] = jacobiRelaxed( A, f, u0, omega )
r_norm=resVec(2:maxiter+1);

figure
plot(x,ua,'kx-',x,u,'bo-')
xlabel('x')
ylabel('u(x)')
legend('analytic','weighted Jacobi')
title('Solution of Au(x) = x(1-x)')

figure
semilogy(r_norm,'-')
grid on
xlabel('iteration')
ylabel('||r||')
title('Residual convergence of weighted Jacobi')

figure
plot(2:maxiter,r_norm(2:maxiter)./r_norm(1:maxiter-1),'-')
xlabel('iteration')
ylabel('conv factor')
title('Convergence factor of weighted Jacobi')
grid on

r_norm_c = r_norm;

%% Weighted Jacobi f = sin(k*pi*x)
for k = 1:4;
f = sin(k*pi*x);
% analytical solution
ua = 1/(k^2*pi^2)*sin(k*pi*x);

maxiter = 1000;
% weighted Jacobi
omega = 1;
[ u, flag, relRes, iter, resVec ] = jacobiRelaxed( A, f, u0, omega )
r_norm(:,k)=resVec(2:maxiter+1);

subplot(2,2,k),
plot(x,ua,'kx-',x,u,'bo-')
grid on
xlabel('x')
ylabel('u(x)')
legend('analytic','weighted Jacobi')
title(sprintf('Solution of Au(x) = sin(%g\\pix)',k))
end

figure
semilogy(1:maxiter,r_norm,'-',1:maxiter,r_norm_c,'--k')
grid on
xlabel('iteration')
ylabel('||r||')
legend('Au(x) = sin(\pix)','Au(x) = sin(2\pix)','Au(x) = sin(3\pix)','Au(x) = sin(4\pix)','Au(x) = x(1-x)')
title('Residual convergence of weighted Jacobi')
grid on

figure
plot(2:maxiter,r_norm(2:maxiter,:)./r_norm(1:maxiter-1,:),'-',2:maxiter,r_norm_c(2:maxiter)./r_norm_c(1:maxiter-1),'--k')
xlabel('iteration')
ylabel('conv factor')
legend('f = sin(\pix)','f = sin(2\pix)','f = sin(3\pix)','f = sin(4\pix)','f = x(1-x)')
title('Convergence factor of weighted Jacobi')
grid on
