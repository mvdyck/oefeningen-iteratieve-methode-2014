function [flag,it,resvec,relresvec] ...
  = gmresILU0Precond( A, b, restart, tol, maxit );

  setup.type = 'nofill';
  [L,U] = ilu(A,setup);  % or [L,U] = luinc(A,0);

  [u,flag,relres,it,resvec] ...
       = gmres( A, b, restart, tol, maxit, L, U );

  it = restart*(it(1)-1) + it(2);
  
  relresvec = resvec / norm( U\(L\b),2 );
end
