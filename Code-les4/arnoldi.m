% ===========================================================
% *** FUNCTION arnoldi
% ***
% *** Stabilized Arnoldi-iteration.
% ***
% ===========================================================
function [V,H] = arnoldi( A, r0, m )

  N = length(r0);

  if nargin<3, m = N; end

  V = zeros(N,m);
  H = zeros(m,m);

  V(:,1) = r0 / norm(r0,2);

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  for i = 1:m-1
      w = A*V(:,i);
      for j=1:i
          H(j,i) = V(:,j)'*w;
          w      = w - H(j,i)*V(:,j);
      end

      H(i+1,i) = norm(w,2);
      V(:,i+1) = w / H(i+1,i);
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  % Do the last step separately.
  % -- This is just updating H(:,m).
  w = A*V(:,m);
  for j = 1:m
      H(j,m) = V(:,j)'*w;
      w      = w - H(j,m)*V(:,j);
  end

end
% ===========================================================
% *** END FUNCTION arnoldi
% ===========================================================
