%% upload files
!git init
!git remote add origin https://mvdyck@bitbucket.org/mvdyck/im-ol3a.git
!git push origin master
!git help push

%% change master to opgave
!git clone https://uauser@bitbucket.org/uauser/im-ol3a.git -b opgave
cd im-ol3
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin opgave:master -f
%% change master to cg
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin cg:master -f
%% change master to cg_rechterhand
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg_rechterhand
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin cg_rechterhand:master -f
%% change master to all
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b all
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin all:master -f

%% retreive oef1
!git clone https://uauser@bitbucket.org/uauser/im-ol1.git

%% retreive oef2
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git

%% retreive oef3a
!git clone https://uauser@bitbucket.org/uauser/im-ol3a.git -b opgave
!git clone https://uauser@bitbucket.org/uauser/im-ol3a.git -b oef3a
!git clone https://uauser@bitbucket.org/uauser/im-ol3a.git -b oef3c

%!git clone https://uauser@bitbucket.org/mvdyck/im-ol3a.git

%%
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b Poisson2D
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg_rechterhand
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b all

