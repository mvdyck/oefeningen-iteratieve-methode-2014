N=100;
[A,b] = Poisson2D( N );
maxiter = 5000;
[u , res ,rho] = cg(A,b,maxiter);
% toon de oplossing
figure(5);imagesc((reshape(u,100,100)))
%% toon de convergentie
figure(6);set(gcf,'Name','convergentie');
semilogy(sqrt(rho),'x')
xlabel('k')
ylabel('||r||_2')
rho

%% Eerste stap cg is res = b-Ax en x =0 => res = b 
h = 1/(N+1);
x=h*(1:N);
y=h*(1:N);
% righthandside: eigenvector with largest eigenvalue
b = kron(sin(N*x*pi),sin(N*y*pi))'; % (eq. 1.14, 1.15)
% ook numeriek te bereken bv via: [V D] = eigs(A,10); b=V(:,1);
figure('Name','rhs poisson');imagesc(abs(reshape(b,N,N)))
[u , res , rho] = cg(A,b,maxiter);
 %%
figure(6);
hold all
semilogy(sqrt(rho),'o')
rho

%%
% righthandside: eigenvector with smallest eigenvalue
% b = kron(sin(x*pi),sin(y*pi))';
% [u , res , rho] = cg(A,b,maxiter);
% 
% semilogy(sqrt(rho),'+')
% rho


%%
% righthandside: function symmetric around axes (0.5,y) and (x,0.5)
b = kron(x.*(1-x),y.*(1-y))';
[u , res , rho] = cg(A,b,maxiter);

semilogy(sqrt(rho),'+')
rho


legend('b^{(1)}','b^{(2)}','b^{(3)}')