% =========================================================================
% *** FUNCTION plotPoisson2D
% ***
% *** Plots the solution of the Poisson equation on the unit square
% *** where the grid points are numbered rowwise starting at (0,0).
% *** Also, the 0-Dirichlet boundary values are plotted.
% ***
% =========================================================================
function plotPoisson2D( u )

  xmin = 0;
  xmax = 1;

  ymin = 0;
  ymax = 1;

  n = length(u);

  N = sqrt(n);

  % set the grid
  h = 1/(N+1);
  x = (0:h:1);
  y = x;

  % Reshape the values back to a matrix.
  % Make use of the assumption that the nodes are numbered rowwise here.
  Z = reshape( u, N, N )';
  z = zeros(N,1);

  % append the 0-Dirichlet boundary values
  Z = [ 0, z', 0;
        z, Z , z;
        0, z', 0 ];

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % Go plot it!
  contourLevels = 64;
  h = contourf( x, y, Z, contourLevels, 'LineStyle', 'none' );

  axis([xmin xmax ymin ymax]);

  daspect([1 1 1]);

  colormap jet;
  colorbar;

  set(gca,'XTick',[0,0.5,1])
  set(gca,'YTick',[0,0.5,1])
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end
% =========================================================================
% *** END FUNCTION plotPoisson2D
% =========================================================================