% =======================================================
% *** FUNCTION gmresPoissonPrecond
% =======================================================
function [flag,it,resvec,relresvec] ...
      = gmresPoissonPrecond( A, b, restart, tol, maxit );

  [u,flag,relres,it,resvec] ...
= gmres( A, b, restart, tol, maxit, @fastPoissonSolver );

  it = restart*(it(1)-1) + it(2);

  relresvec = resvec / norm( fastPoissonSolver(b),2 );

  % -----------------------------------------------------
  % *** FUNCTION fastPoissonSolver
  % ***
  % *** Solves the 2D Poisson equation with homogeneous
  % *** Dirichlet boundary conditions quickly.
  % ***
  % -----------------------------------------------------
  function u = fastPoissonSolver( f )

    Nx = sqrt( length(f) );
    hx = 1/(Nx+1);

    f = hx^2 * f;

    % poicalc sits in MATLAB's PDE toolbox
    u = poicalc( f );

  end
  % -----------------------------------------------------
  % *** END FUNCTION fastPoissonSolver
  % -----------------------------------------------------

end
% =======================================================
% *** END FUNCTION gmresPoissonPrecond
% =======================================================