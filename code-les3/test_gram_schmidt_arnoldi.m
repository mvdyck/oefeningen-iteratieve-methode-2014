
% Gram-Schmidt-orthonormalisatie
N=100;
[A,b] = getPoissonMatrix( N );
x_0=rand(N,1);
r=b-A*x_0;
k=100;
v=zeros(N,k);
v(:,1)=r/norm(r,2);
for i=2:k
     som=0;
     for j=1:(i-1)
         som=som+((A*v(:,i-1))'*v(:,j))*v(:,j);
     end
     v(:,i)=A*v(:,i-1)-som;
     v(:,i)=v(:,i)/norm(v(:,i),2);
end

%%
% Gestabiliseerde Gram-Schmidt-orthonormalisatie

v2 = zeros(N,k);
v2(:,1)=r/norm(r,2);
for i=2:k
      v2(:,i) = A*v2(:,i-1);
      for j=1:(i-1)
          v2(:,i) = v2(:,i) - (v2(:,i)'*v2(:,j))*v2(:,j);
      end
      v2(:,i)=v2(:,i)/norm(v2(:,i),2);
end
