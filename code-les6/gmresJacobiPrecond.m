% =======================================================
% *** FUNCTION gmresJacobiPrecond
% =======================================================
function [flag,it,resvec,relresvec] ...
       = gmresJacobiPrecond( A, b, restart, tol, maxit );

  n = size(A,1);
  M = spdiags( diag(A), 0, n, n );

  [u,flag,relres,it,resvec] ...
                 = gmres( A, b, restart, tol, maxit, M );

  it = restart*(it(1)-1) + it(2);

  relresvec = resvec / norm( M\b,2 );

end
% =======================================================
% *** END FUNCTION gmresJacobiPrecond
% =======================================================