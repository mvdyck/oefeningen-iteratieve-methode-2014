% =========================================================================
% *** FUNCTION getResults
% =========================================================================
function getResults()

  % set the pde parameters
  f  = 1;
  a0 = [0.1,-0.2];
  a  = 1e0*a0;

  Nx = 100;

%   % setup the parallel environment
%   if ~matlabpool('size')
%       matlabpool
%   end

  comparePreconditionersRes( Nx, a, f );
  comparePreconditionersN( a, f );

end
% =========================================================================
% *** FUNCTION getResults
% =========================================================================



% =========================================================================
% *** FUNCTION comparePreconditionersRes
% ***
% *** Pick one particular problem size parameter Nx and have different
% *** preconditioners run over it. Plot the sequence of the residual
% *** 2-norms.
% ***
% =========================================================================
function comparePreconditionersRes( Nx, a, f )

  hx = 1/(Nx+1);

  % get the matrix and the right hand side
  [A,b] = getPoissonConvection2D( Nx, a, f );
  N = length(b);

  % set the solvers
  solvers = { @gmresNoPrecond, ...
              @gmresJacobiPrecond, ...
              @gmresILU0Precond, ...
              @gmresPoissonPrecond ...
            };

  % set the common solver parameters
  restart = 30;
  tol     = 1e-11;
  maxit   = 500;

  % -----------------------------------------------------------------------
  % let the solvers run
  parfor l = 1:length(solvers)
      [flag,it,resvec{l},relresvec{l}] = solvers{l}( A, b, restart, tol, maxit );
      flag
  end
  % -----------------------------------------------------------------------


  colors  = 'rgbk';
  markers = '+ox*';
  % -----------------------------------------------------------------------
  % plot resvec
  figure;
  hold on;
  for l = 1:length(solvers)
      n(l) = length(resvec{l});
      skip = ceil(n(l)/100);
      plot( (0:skip:n(l)-1), resvec{l}(1:skip:end), [colors(l) markers(l)] );
  end
  hold off;

  set( gca, 'YScale', 'log' );
  legend( {'--','Jacobi','ILU(0)','Poisson'}, 'Location', 'NorthEast' );
  axis([ 0 max(n) 1e-11 1e4 ] );
  xlabel('k')
  ylabel('\norm{r^{(k)}}_2')
  % -----------------------------------------------------------------------

  % -----------------------------------------------------------------------
  % plot the relresvec
  figure;
  hold on;
  for l = 1:length(solvers)
      n(l) = length(relresvec{l});
      skip = ceil(n(l)/100);
      plot( (0:skip:n(l)-1), relresvec{l}(1:skip:end), [colors(l) markers(l)] );
  end
  hold off;

  set( gca, 'YScale', 'log' );
  legend( {'--','Jacobi','ILU(0)','Poisson'}, 'Location', 'NorthEast' );
  axis([ 0 max(n) 1e-11 1e2 ] );
  xlabel('k')
  ylabel('\norm{r^{(k)}}_2 / \norm{M^{-1}b}_2')
  % -----------------------------------------------------------------------

end
% =========================================================================
% *** END FUNCTION comparePreconditionersRes
% =========================================================================



% =========================================================================
% *** FUNCTION comparePreconditionersN
% ***
% *** Plots the execution TIME for different preconditioners over the
% *** problem size parameter Nx.
% ***
% =========================================================================
function comparePreconditionersN( a, f )

  minN  = 20;
  skipN = 20;
  maxN  = 200;

  n = ceil( (maxN - minN)/skipN );

  % set the solvers
  solvers = { @gmresNoPrecond, ...
              @gmresJacobiPrecond, ...
              @gmresILU0Precond, ...
              @gmresPoissonPrecond ...
            };

  numSolvers = length(solvers);

  % set the common solver parameters
  restart = 30;
  tol     = 1e-11;
  maxit   = 500;

  % allocate the stats containers
  sampleLength = length(minN:skipN:maxN);
  NN   = cell( numSolvers, 1 );
  time = cell( numSolvers, 1 );
  iter = cell( numSolvers, 1 );
  for k = 1:numSolvers
      NN{k}   = zeros(sampleLength,1);
      time{k} = zeros(sampleLength,1);
      iter{k} = zeros(sampleLength,1);
  end

  Nx = minN:skipN:maxN;
  for k = 1:sampleLength

      disp( sprintf( 'Calculating N=%d...\n', Nx(k) ));

      % get full matrix
      hx = 1/(Nx(k)+1);
      [A,b] = getPoissonConvection2D( Nx(k), a, f );

      % -------------------------------------------------------------------
      % let the solvers run for the particular Nx
      parfor l = 1:numSolvers

          tic
          [flag,it] = solvers{l}( A, b, restart, tol, maxit );
          t = toc;

          NN{l}(k) = Nx(k);
          if ~flag
              time{l}(k) = t;
              iter{l}(k) = it;
          else
              flag
          end
      end
      % -------------------------------------------------------------------

  end

  colors  = 'rgbk';
  markers = '+ox*';

  % -----------------------------------------------------------------------
  % plot the iterations
  figure;
  hold on;
  for l = 1:numSolvers
      plot( NN{l}, iter{l}, [colors(l) markers(l)] );
  end
  hold off;

  legend( {'--','Jacobi','ILU(0)','Poisson'}, 'Location', 'NorthWest' );
  xlabel('N')
  ylabel('k_{\varepsilon}')
  % -----------------------------------------------------------------------


  % -----------------------------------------------------------------------
  % plot the times
  figure;
  hold on;
  for l = 1:numSolvers
      plot( NN{l}, time{l}, [colors(l) markers(l)] );
  end
  hold off;

  legend( {'--','Jacobi','ILU(0)','Poisson'}, 'Location', 'NorthWest' );
  xlabel('N')
  ylabel('t_{\varepsilon}')
  % -----------------------------------------------------------------------

end
% =========================================================================
% *** END FUNCTION comparePreconditionersN
% =========================================================================