function  [u , res, rho] = cg(A,b,maxiter)
N = length(b) ;

% initialize the matrices that hold the vectors.
res  = zeros(N,1);
u  = zeros(N,1);
w  = zeros(N,1);
p = zeros(N,1);
 

rho = zeros(maxiter+1,1);
tol     = 1e-10;
normB   = norm( b, 2 );
relTol = tol*normB;
 
% set the initial residu to the righthand side/
res= b-A*u;
% calculate the initial rho
rho(1) = norm(res)^2;

for k=1:maxiter
     
      if norm(res,2) <= relTol
         break
      end
     
      
     % the first search direction is in the direction of r_0
     if k-1 < 1
          p = res;
     else
         % recurrence for the search directions
          beta = rho(k)/rho(k-1);
          p = res + beta*p;
     end
     w = A*p;
     alpha = rho(k)/(p'*w);
     u = u + alpha*p;
     res = res - alpha*w;
     rho(k+1) = norm(res)^2;
end
    
% chop down rho (was allocated with length maxiter)
rho=rho(1:k+1);
    
        