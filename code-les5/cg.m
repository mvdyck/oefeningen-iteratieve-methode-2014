% ===========================================================
% *** FUNCTION cg
% ===========================================================
function [ u, flag, relRes, iter, resVec ] = ...
                                   cg( A, b, tol, maxIt, u0 )

  % initialization: handle optional argumentss
  n = length(b);
  if nargin<5, u0    = zeros(n,1); end
  if nargin<4, maxIt = min(n,20);  end
  if nargin<3, tol   = 1e-6;       end

  u = u0;
  r = b - A*u;

  normB  = norm( b, 2 );

  % allocate resVec
  resVec    = zeros( maxIt+1, 1 );
  resVec(1) = norm( r, 2 );

  % set default flag: everything alright
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIt

      if resVec(iter) <= relTol
          break
      end

      % get next search direction
      if iter==1
          p = r;
      else
          beta = (resVec(iter) / resVec(iter-1))^2;
          p    = r + beta*p;
      end

      w     = A*p;
      alpha = (resVec(iter))^2 / (p'*w);

      % the actual update
      alphap = alpha*p;
      u = u + alphap;
      r = r - alpha*w;

      resVec(iter+1) = norm(r,2);

      if norm(alphap,2)<1e-15
          flag = 2;
          warning ( 'cg:stag', ...
                    'Two successive itns. where the same.' );
          break;
      end

  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % handle the output
  if iter>=maxIt
      flag = 1;
      warning ( 'cg:maxIter', ...
                'Maximum number of iterations reached.' );
  end
  relRes = resVec(iter+1)/normB;

  % chop down resVec (was allocated with length maxIt)
  resVec = resVec(1:iter+1);
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end
% ===========================================================
% *** END FUNCTION cg
% ===========================================================