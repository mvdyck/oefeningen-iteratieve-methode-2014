% ===================================================
% *** FUNCTION cg
% ***
% *** Conjugate gradient method.
% ***
% ===================================================
function [ u, flag, relRes, iter, resVec2, errVecA ] ...
                   = cg( A, b, tol, maxIt, u0, uSol )

  % initialization: handle optional arguments
  n = length(b);
  if nargin == 6
      exactMode = 1;
      errVecA   = zeros( maxIt+1, 1 );
  else
      exactMode = 0;
  end
  if nargin < 5, u0    = zeros(n,1); end
  if nargin < 4, maxIt = min(n,20);  end
  if nargin < 3, tol   = 1e-6;       end

  u = u0;
  r = b - A*u;

  normB  = norm( b, 2 );

  % allocate resVec2
  resVec2    = zeros( maxIt+1, 1 );
  resVec2(1) = norm( r, 2 );

  if exactMode
       err = uSol - u;
       errVecA(1) = sqrt( err'*r );
  end

  % set default flag: everything all right
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - -
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIt

      if resVec2(iter) <= relTol
          break
      end

      % get next search direction
      if iter==1
          p = r;
      else
          beta = (resVec2(iter) / resVec2(iter-1))^2;
          p    = r + beta*p;
      end

      w     = A*p;
      alpha = (resVec2(iter))^2 / (p'*w);

      % the actual update
      alphap = alpha*p;
      u = u + alphap;
      r = r - alpha*w;

      resVec2(iter+1) = norm(r,2);
      if exactMode
          err = uSol - u;
          errVecA(iter+1) = sqrt( err'*r );
      end

      if norm(alphap,2)<tol
          flag = 2;
          warning ( 'cg:stag', ...
                    'Two succ. itns. where equal.' );
          break;
      end

  end
  % - - - - - - - - - - - - - - - - - - - - - - - - -


  % - - - - - - - - - - - - - - - - - - - - - - - - -
  % handle the output
  if iter>=maxIt
      flag = 1;
      warning ( 'cg:maxIter', ...
           'Maximum number of iterations reached.' );
  end
  relRes = resVec2(iter+1)/normB;

  % chop down resVec2
  % (was allocated with length maxIt)
  resVec2 = resVec2(1:iter+1);
  if exactMode
      errVecA = errVecA(1:iter+1);
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - -
end
% ===================================================
% *** END FUNCTION cg
% ===================================================