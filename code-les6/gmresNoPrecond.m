% =======================================================
% *** FUNCTION gmresNoPrecond
% =======================================================
function [flag,it,resvec,relresvec] ...
           = gmresNoPrecond( A, b, restart, tol, maxit );

  [u,flag,relres,it,resvec] = ...
                      gmres( A, b, restart, tol, maxit );
                  
  it = restart*(it(1)-1) + it(2);

  relresvec = resvec / norm(b,2);

end
% =======================================================
% *** END FUNCTION gmresNoPrecond
% =======================================================