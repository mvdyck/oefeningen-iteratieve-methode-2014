%% upload files
!git init
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git add Poisson2D.m
!git commit -m "Poisson2D.m"
!git push origin master
!git help push

%% change master to Poisson2D
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b Poisson2D
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin Poisson2D:master -f
%% change master to cg
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin cg:master -f
%% change master to cg_rechterhand
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg_rechterhand
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin cg_rechterhand:master -f
%% change master to all
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b all
cd im-ol2
!git remote remove origin
!git remote add origin https://mvdyck@bitbucket.org/uauser/im-ol2.git
!git push origin all:master -f

%% retreive oef1
!git clone https://uauser@bitbucket.org/uauser/im-ol1.git

%% retreive oef2
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git

%% retreive oef2
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git

%%
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b Poisson2D
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b cg_rechterhand
!git clone https://uauser@bitbucket.org/uauser/im-ol2.git -b all

