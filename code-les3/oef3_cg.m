e = ones(3,1);
A = spdiags([-e 2*e -e],[-1 0 1],3,3);
b = [0;0;1];

maxiter = 3;
[x,r,w ,p,rho] = mycg(A,b,maxiter)

figure
daspect([1 1 1])
view(-37.5,30)
grid on
title('Results of CG iterations')
xlabel('x')
ylabel('y')
zlabel('z')
hold on

for ind = 1:3
% residuals r
plot3([0 r(1,ind)],[0 r(2,ind)],[0 r(3,ind)])
text(r(1,ind)*0.7,r(2,ind)*0.7,r(3,ind)*0.7,sprintf('  r_%d',ind))
% search directions p
plot3([0 p(1,ind+1)],[0 p(2,ind+1)],[0 p(3,ind+1)],'r')
text(p(1,ind+1)*0.75,p(2,ind+1)*0.75,p(3,ind+1)*0.75,sprintf('  p_%d',ind))
% (approximate) solutions x
plot3(x(1,ind+1),x(2,ind+1),x(3,ind+1),'kx')
text(x(1,ind+1),x(2,ind+1),x(3,ind+1),sprintf('   x_%d',ind))
end

P = p(:,2:end);
P.'*A*P
