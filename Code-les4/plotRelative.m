
% ===========================================================
function plotRelative( x, y )

  % Plot then versus `minEigJ`
  tmpX = x(1:end-1) + 0.5;
  tmpY = y(2:end) ./ y(1:end-1);
  plot( tmpX, tmpY, 'or' );

  axis([min(x) max(x) 0 1])

end
% ===========================================================
% *** END FUNCTION plotRelative
% ===========================================================